<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek;

use Amass\Cdek\config\ConfigInterface;
use Amass\Cdek\requests\JsonRequestInterface;
use Amass\Cdek\requests\ParamRequestInterface;
use Amass\Cdek\requests\RequestInterface;
use Amass\Cdek\responses\ResponseInterface;
use GuzzleHttp\Client as Guzzle;
use JMS\Serializer\Handler\DateHandler;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;

/**
 * Class CdekClient
 * @package Amass\Cdek
 */
class CdekClient
{
  /**
   * @var string
   */
  private $account;
  /**
   * @var string
   */
  private $secure;
  /**
   * @var Guzzle
   */
  private $http;
  /**
   * @var \JMS\Serializer\Serializer
   */
  private $serializer;
  /**
   * @var ConfigInterface
   */
  private $config;

  /**
   * CdekClient constructor.
   * @param string $account
   * @param string $secure
   * @param ConfigInterface $config
   */
  public function __construct($account, $secure, ConfigInterface $config)
  {
    $this->account = $account;
    $this->secure = $secure;

    $this->http = new Guzzle();
    $this->serializer = SerializerBuilder::create()
      ->setDebug(true)
      ->configureHandlers(function (HandlerRegistry $registry) {
      $registry->registerSubscribingHandler(new DateHandler());
    })
      ->build();
    $this->config = $config;
  }

  /**
   * @param $request
   * @return ResponseInterface
   * @throws \Exception
   */
  public function process(RequestInterface $request)
  {
    $class = get_class($request);
    $response = $this->sendRequest($request);
    foreach ($this->config->getMap() as $dataType => $map) {
      if (array_key_exists($class, $map)) {
        return $this->serializer->deserialize($response, $map[$class], $dataType);
      }
    }
    throw new \Exception("Class [$class] not mapped.");
  }

  /**
   * @param RequestInterface $request
   * @return string
   */
  private function sendRequest(RequestInterface $request)
  {
    $this->initRequest($request);
    $response = $this->http->request(
      $request->getMethod(),
      $request->getAddress(),
      $this->extractOptions($request)
    );
    return $response->getBody()->getContents();
  }

  /**
   * @param $request
   * @return array
   */
  private function extractOptions($request)
  {
    if ($request instanceof ParamRequestInterface) {
      return ['query' => $request->getParams()];
    }
//    if ($request instanceof CdekXmlRequest) {
//      return ['form_params' => ['xml_request' => $this->serializer->serialize($request, 'xml')]];
//    }
    if ($request instanceof JsonRequestInterface) {
      return [
        'body'    => json_encode($request->getBody()),
        'headers' => ['Content-Type' => 'application/json'],
      ];
    }
    return [];
  }

  /**
   * @param RequestInterface $request
   */
  private function initRequest(RequestInterface $request)
  {
    $date = new \DateTimeImmutable();
    $request->date($date)->credentials($this->account, $this->getSecure($date));
  }

  /**
   * @param \DateTimeInterface $date
   * @return string
   */
  private function getSecure(\DateTimeInterface $date)
  {
    return md5($date->format('Y-m-d')."&{$this->secure}");
  }
}