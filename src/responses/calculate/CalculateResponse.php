<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\responses\calculate;

use Amass\Cdek\responses\ResponseInterface;
use JMS\Serializer\Annotation as JMS;

/**
 * Class CalculateResponse
 * @package Amass\Cdek\responses\calculate
 */
class CalculateResponse implements ResponseInterface
{
  /**
   * @JMS\SerializedName("result")
   * @JMS\Type("Amass\Cdek\responses\calculate\ResultResponse")
   *
   * @var ResultResponse
   */
  protected $result;
  /**
   * @JMS\SerializedName("error")
   * @JMS\Type("array<Amass\Cdek\responses\calculate\ErrorResponse>")
   *
   * @var array|ErrorResponse[]
   */
  public $errors = [];

  /**
   * @return bool
   */
  public function hasErrors()
  {
    return ! empty($this->errors);
  }

  /**
   * @return array
   */
  public function getErrors()
  {
    return $this->errors;
  }

  /**
   * @return ResultResponse
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * @param $name
   * @param $arguments
   * @return mixed
   */
  public function __call($name, $arguments)
  {
    if ($this->result && method_exists($this->result, $name)) {
      return $this->result->{$name}(...$arguments);
    }
    $class = static::class;
    throw new \BadMethodCallException("Method [$name] not found in [$class].");
  }
}