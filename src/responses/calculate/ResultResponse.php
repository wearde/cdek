<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\responses\calculate;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ResultResponse
 * @package app\modules\cdek\src\responses\calculate
 */
class ResultResponse
{
  /**
   * @JMS\SerializedName("price")
   * @JMS\Type("float")
   *
   * @var null|float
   */
  protected $price;
  /**
   * @JMS\SerializedName("deliveryPeriodMin")
   * @JMS\Type("int")
   *
   * @var null|int
   */
  protected $deliveryPeriodMin;
  /**
   * @JMS\SerializedName("deliveryPeriodMax")
   * @JMS\Type("int")
   *
   * @var null|int
   */
  protected $deliveryPeriodMax;
  /**
   * @JMS\SerializedName("deliveryDateMin")
   * @JMS\Type("DateTimeImmutable<'Y-m-d'>")
   *
   * @var null|\DateTimeImmutable
   */
  protected $deliveryDateMin;
  /**
   * @JMS\SerializedName("deliveryDateMax")
   * @JMS\Type("DateTimeImmutable<'Y-m-d'>")
   *
   * @var null|\DateTimeImmutable
   */
  protected $deliveryDateMax;
  /**
   * @JMS\SerializedName("tariffId")
   * @JMS\Type("int")
   *
   * @var int
   */
  protected $tariffId;
  /**
   * @JMS\SerializedName("priceByCurrency")
   * @JMS\Type("float")
   *
   * @var null|float
   */
  protected $priceByCurrency;
  /**
   * @JMS\SerializedName("currency")
   * @JMS\Type("string")
   *
   * @var null|string
   */
  protected $currency;

  /**
   * @return float|null
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * @return int|null
   */
  public function getDeliveryPeriodMin()
  {
    return $this->deliveryPeriodMin;
  }

  /**
   * @return int|null
   */
  public function getDeliveryPeriodMax()
  {
    return $this->deliveryPeriodMax;
  }

  /**
   * @return int|null
   */
  public function getTariffId()
  {
    return $this->tariffId;
  }

  /**
   * @return float|null
   */
  public function getPriceByCurrency()
  {
    return $this->priceByCurrency;
  }

  /**
   * @return null|string
   */
  public function getCurrency()
  {
    return $this->currency;
  }

  /**
   * @return \DateTimeImmutable|null
   */
  public function getDeliveryDateMin()
  {
    return $this->deliveryDateMin;
  }

  /**
   * @return \DateTimeImmutable|null
   */
  public function getDeliveryDateMax()
  {
    return $this->deliveryDateMax;
  }
}