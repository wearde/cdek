<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\responses\calculate;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ErrorResponse
 * @package Amass\Cdek\responses\calculate
 */
class ErrorResponse
{
  /**
   * @JMS\Type("string")
   *
   * @var string
   */
  protected $text;
  /**
   * @JMS\Type("int")
   *
   * @var int
   */
  protected $code;

  /**
   * @return string
   */
  public function getText()
  {
    return $this->text;
  }

  /**
   * @return int
   */
  public function getCode()
  {
    return $this->code;
  }
}