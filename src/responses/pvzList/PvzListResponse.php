<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\responses\pvzList;

use Amass\Cdek\entity\models\Pvz;
use JMS\Serializer\Annotation as JMS;
/**
 * Class PvzListResponse
 * @package Amass\Cdek\responses\pvzList
 * @JMS\XmlRoot("PvzList")
 */
class PvzListResponse
{
  /**
   * @JMS\XmlList(entry="Pvz", inline=true)
   * @JMS\Type("array<Amass\Cdek\entity\models\Pvz>")
   *
   * @var Pvz[];
   */
  private $pvzs = [];

  /**
   * @return Pvz[]
   */
  public function getPvzs()
  {
    return $this->pvzs;
  }
}