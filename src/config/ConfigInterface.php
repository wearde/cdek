<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\config;

/**
 * Interface ConfigInterface
 * @package Amass\Cdek\config
 */
interface ConfigInterface
{
  /**
   * @return array
   */
  public function getMap();
}