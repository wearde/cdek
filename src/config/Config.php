<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\config;

use Amass\Cdek\requests\calculator\CalculatorRequest;
use Amass\Cdek\requests\pvzList\PvzListRequest;
use Amass\Cdek\responses\calculate\CalculateResponse;
use Amass\Cdek\responses\pvzList\PvzListResponse;

class Config implements ConfigInterface
{
  /**
   * @return array
   */
  public function getMap()
  {
    return [
      'xml'  => [
        PvzListRequest::class => PvzListResponse::class,
      ],
      'json' => [
        CalculatorRequest::class => CalculateResponse::class,
      ],
    ];
  }
}