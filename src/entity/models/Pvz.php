<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\entity\models;

use Amass\Cdek\entity\models\common\OfficeHowGo;
use Amass\Cdek\entity\models\common\OfficeImage;
use Amass\Cdek\entity\models\common\WeightLimit;
use Amass\Cdek\entity\models\common\WorkTimeY;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Pvz
 * @package Amass\Cdek\entity\models
 */
class Pvz
{
  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Code")
   * @JMS\Type("string")
   *
   * @var int
   */
  private $Code;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Type")
   * @JMS\Type("string")
   *
   * @var int
   */
  private $Type;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Name")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $Name;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("CityCode")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $CityCode;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("City")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $City;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("WorkTime")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $WorkTime;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Address")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $Address;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Phone")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $Phone;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Note")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $Note;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("coordX")
   * @JMS\Type("float")
   *
   * @var float
   */
  private $coordX;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("coordY")
   * @JMS\Type("float")
   *
   * @var float
   */
  private $coordY;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("CountryCode")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $CountryCode;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("RegionCode")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $RegionCode;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("RegionName")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $RegionName;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("FullAddress")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $FullAddress;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("IsDressingRoom")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $IsDressingRoom;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("HaveCashless")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $HaveCashless;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("AllowedCod")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $AllowedCod;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("OwnerCode")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $OwnerCode;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("NearestStation")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $NearestStation;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("MetroStation")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $MetroStation;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("Site")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $Site;

  /**
   * @JMS\XmlList(entry="WeightLimit", inline=true)
   * @JMS\Type("array<Amass\Cdek\entity\models\common\WeightLimit>")
   *
   * @var  WeightLimit[]
   */
  private $WeightLimit = [];

  /**
   * @JMS\XmlList(entry="WorkTimeY", inline=true)
   * @JMS\Type("array<Amass\Cdek\entity\models\common\WorkTimeY>")
   *
   * @var WorkTimeY[];
   */
  private $WorkTimeY = [];

  /**
   * @JMS\XmlList(entry="OfficeImage", inline=true)
   * @JMS\Type("array<Amass\Cdek\entity\models\common\OfficeImage>")
   *
   * @var OfficeImage[];
   */

  private $OfficeImage = [];

  /**
   * @JMS\XmlList(entry="OfficeHowGo", inline=true)
   * @JMS\Type("array<Amass\Cdek\entity\models\common\OfficeHowGo>")
   *
   * @var OfficeHowGo[];
   */
  private $OfficeHowGo = [];

  /**
   * @return int
   */
  public function getCode()
  {
    return $this->Code;
  }

  /**
   * @return int
   */
  public function getType()
  {
    return $this->Type;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->Name;
  }

  /**
   * @return int
   */
  public function getCityCode()
  {
    return $this->CityCode;
  }

  /**
   * @return string
   */
  public function getCity()
  {
    return $this->City;
  }

  /**
   * @return string
   */
  public function getWorkTime()
  {
    return $this->WorkTime;
  }

  /**
   * @return string
   */
  public function getAddress()
  {
    return $this->Address;
  }

  /**
   * @return string
   */
  public function getPhone()
  {
    return $this->Phone;
  }

  /**
   * @return string
   */
  public function getNote()
  {
    return $this->Note;
  }

  /**
   * @return float
   */
  public function getCoordX()
  {
    return $this->coordX;
  }

  /**
   * @return float
   */
  public function getCoordY()
  {
    return $this->coordY;
  }

  /**
   * @return int
   */
  public function getCountryCode()
  {
    return $this->CountryCode;
  }

  /**
   * @return int
   */
  public function getRegionCode()
  {
    return $this->RegionCode;
  }

  /**
   * @return string
   */
  public function getRegionName()
  {
    return $this->RegionName;
  }

  /**
   * @return string
   */
  public function getFullAddress()
  {
    return $this->FullAddress;
  }

  /**
   * @return string
   */
  public function getisDressingRoom()
  {
    return $this->IsDressingRoom;
  }

  /**
   * @return string
   */
  public function getHaveCashless()
  {
    return $this->HaveCashless;
  }

  /**
   * @return int
   */
  public function getAllowedCod()
  {
    return $this->AllowedCod;
  }

  /**
   * @return string
   */
  public function getOwnerCode()
  {
    return $this->OwnerCode;
  }

  /**
   * @return string
   */
  public function getNearestStation()
  {
    return $this->NearestStation;
  }

  /**
   * @return string
   */
  public function getMetroStation()
  {
    return $this->MetroStation;
  }

  /**
   * @return string
   */
  public function getSite()
  {
    return $this->Site;
  }

  /**
   * @return WeightLimit[]
   */
  public function getWeightLimit()
  {
    return array_shift($this->WeightLimit);
  }

  /**
   * @return WorkTimeY[]
   */
  public function getWorkTimeY()
  {
    return $this->WorkTimeY;
  }

  /**
   * @return OfficeImage[]
   */
  public function getOfficeImage()
  {
    return $this->OfficeImage;
  }

  /**
   * @return OfficeHowGo[]
   */
  public function getOfficeHowGo()
  {
    return array_shift($this->OfficeHowGo);
  }
}
