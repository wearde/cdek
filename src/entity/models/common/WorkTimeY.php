<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\entity\models\common;
use JMS\Serializer\Annotation as JMS;

class WorkTimeY
{
  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("day")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $day;

  /**
   * @return int
   */
  public function getDay()
  {
    return $this->day;
  }

  /**
   * @return string
   */
  public function getPeriods()
  {
    return $this->periods;
  }

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("periods")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $periods;
}
