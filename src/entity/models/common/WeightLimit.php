<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\entity\models\common;

use JMS\Serializer\Annotation as JMS;

/**
 * Class WeightLimit
 * @package Amass\Cdek\entity\models\common
 */
class WeightLimit
{
  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("WeightMin")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $WeightMin;

  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("WeightMax")
   * @JMS\Type("int")
   *
   * @var int
   */
  private $WeightMax;

  /**
   * @return int
   */
  public function getWeightMin()
  {
    return $this->WeightMin;
  }

  /**
   * @return int
   */
  public function getWeightMax()
  {
    return $this->WeightMax;
  }
}