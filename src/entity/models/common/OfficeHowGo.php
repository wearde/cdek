<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\entity\models\common;

use JMS\Serializer\Annotation as JMS;

/**
 * Class OfficeHowGo
 * @package Amass\Cdek\entity\models\common
 */
class OfficeHowGo
{
  /**
   * @JMS\XmlAttribute
   * @JMS\SerializedName("url")
   * @JMS\Type("string")
   *
   * @var string
   */
  private $url;

  /**
   * @return string
   */
  public function getUrl()
  {
    return $this->url;
  }
}
