<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\methods;

use Amass\Cdek\requests\RequestInterface;
use Amass\Cdek\responses\ResponseInterface;

/**
 * Interface MethodInterface
 * @package Amass\Cdek\methods
 */
interface MethodInterface
{
  /**
   * @param RequestInterface $request
   * @return ResponseInterface
   */
  public function request(RequestInterface $request);
}