<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\methods;

use Amass\Cdek\CdekClient;
use Amass\Cdek\requests\RequestInterface;
use Amass\Cdek\responses\ResponseInterface;

class Calculate implements MethodInterface
{
  /**
   * @var CdekClient
   */
  private $client;

  /**
   * PvzList constructor.
   * @param CdekClient $client
   */
  public function __construct(CdekClient $client)
  {
    $this->client = $client;
  }

  /**
   * @param RequestInterface $request
   * @return ResponseInterface
   */
  public function request(RequestInterface $request)
  {
    return $this->client->process($request);
  }
}