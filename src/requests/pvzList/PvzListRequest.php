<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\requests\pvzList;

use Amass\Cdek\requests\ParamRequestInterface;
use Amass\Cdek\requests\RequestInterface;

/**
 * Class PvzListRequest
 * @package Amass\Cdek\requests\pvzList
 */
class PvzListRequest implements ParamRequestInterface, RequestInterface
{
  const TYPE_PVZ = 'PVZ';
  const TYPE_ALL = 'ALL';
  const TYPE_POSTOMAT = 'POSTOMAT';
  const METHOD = 'GET';
  const ADDRESS = 'http://int.cdek.ru/pvzlist.php';

  /** @var string */
  protected $type = self::TYPE_PVZ;

  /** @var int */
  protected $cityId;

  /** @var int */
  protected $regionId;

  /** @var int */
  protected $countryId;

  /** @var string */
  protected $cityPostCode;

  /** @var bool */
  protected $cashless;

  /** @var bool */
  protected $dressingRoom;

  /** @var bool */
  protected $codAllowed;

  /** @var int */
  protected $maxWeight;

  /**
   * @param string $type
   * @return $this
   */
  public function setType($type)
  {
    $this->type = $type;
    return $this;
  }

  /**
   * @param int $id
   * @return $this
   */
  public function setCityId($id)
  {
    $this->cityId = $id;
    return $this;
  }

  /**
   * @param int $regionId
   * @return $this
   */
  public function setRegionId($regionId)
  {
    $this->regionId = $regionId;
    return $this;
  }

  /**
   * @param int $countryId
   * @return $this
   */
  public function setCountryId($countryId)
  {
    $this->countryId = $countryId;
    return $this;
  }

  /**
   * @param string $code
   * @return $this
   */
  public function setPostCode($code)
  {
    $this->cityPostCode = $code;
    return $this;
  }

  /**
   * @param bool $cashless
   * @return $this
   */
  public function setCashless($cashless)
  {
    $this->cashless = $cashless;
    return $this;
  }

  /**
   * @param bool $haveDressingRoom
   * @return $this
   */
  public function setDressingRoom($haveDressingRoom)
  {
    $this->dressingRoom = $haveDressingRoom;
    return $this;
  }

  /**
   * @param bool $codAllowed
   * @return $this
   */
  public function setCodAllowed($codAllowed)
  {
    $this->codAllowed = $codAllowed;
    return $this;
  }

  /**
   * @param int $maxWeight
   * @return $this
   */
  public function setMaxWeight($maxWeight)
  {
    $this->maxWeight = $maxWeight;
    return $this;
  }

  /**
   * @return string
   */
  public function getMethod()
  {
    return self::METHOD;
  }

  /**
   * @return string
   */
  public function getAddress()
  {
    return self::ADDRESS;
  }

  /**
   * @return array
   */
  public function getParams()
  {
    return [
      'type' => $this->type,
      'cityid' => $this->cityId,
      'regionid' => $this->regionId,
      'countryid' => $this->countryId,
      'citypostcode' => $this->cityPostCode,
      'havecashles' => $this->cashless,
      'weightmax' => $this->maxWeight,
      'allowedcod' => $this->codAllowed,
      'isdressingroom' => $this->dressingRoom,
    ];
  }

  /**
   * @param \DateTimeInterface $date
   * @return $this
   */
  public function date(\DateTimeInterface $date)
  {
    return $this;
  }

  /**
   * @param string $account
   * @param string $secure
   * @return $this
   */
  public function credentials($account, $secure)
  {
    return $this;
  }
}