<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\requests;

/**
 * Interface CdekRequestInterface
 * @package Amass\Cdek\requests
 */
interface RequestInterface
{
  /**
   * @return string
   */
  public function getMethod();

  /**
   * @return string
   */
  public function getAddress();

  /**
   * @param \DateTimeInterface $date
   * @return RequestInterface
   */
  public function date(\DateTimeInterface $date);

  /**
   * @param string $account
   * @param string $secure
   * @return RequestInterface
   */
  public function credentials($account, $secure);
}