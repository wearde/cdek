<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Cdek\requests\calculator;

use Amass\Cdek\requests\JsonRequestInterface;
use Amass\Cdek\requests\RequestInterface;

/**
 * Class CalculatorRequest
 * @package Amass\Cdek\requests\calculator
 */
class CalculatorRequest implements JsonRequestInterface
{
  const MODE_DOOR_DOOR = 1;

  const MODE_DOOR_WAREHOUSE = 2;

  const MODE_WAREHOUSE_DOOR = 3;

  const MODE_WAREHOUSE_WAREHOUSE = 4;

  const METHOD = 'POST';

  const ADDRESS = 'http://api.cdek.ru/calculator/calculate_price_by_json.php';
  /**
   * @var int
   */
  protected $senderCityId;
  /**
   * @var string
   */
  protected $senderCityPostCode;

  /** @var \DateTime */
  protected $dateExecute;
  /**
   * @var string
   */
  protected $authLogin;
  /**
   * @var string
   */
  protected $secure;
  /**
   * @var int
   */
  protected $receiverCityId;
  /**
   * @var string
   */
  protected $receiverCityPostCode;
  /**
   * @var int
   */
  protected $tariffId;
  /**
   * @var array
   */
  protected $tariffList = [];
  /**
   * @var int
   */
  protected $modeId;
  /**
   * @var array
   */
  protected $goods;

  /**
   * @param \DateTimeInterface $date
   * @return RequestInterface
   */
  public function date(\DateTimeInterface $date)
  {
    $this->dateExecute = $date;
    return $this;
  }

  /**
   * @param string $account
   * @param string $secure
   * @return RequestInterface
   */
  public function credentials($account, $secure)
  {
    $this->authLogin = $account;
    $this->secure = $secure;
    return $this;
  }

  /**
   * @return string
   */
  public function getAddress()
  {
    return static::ADDRESS;
  }

  /**
   * @return string
   */
  public function getMethod()
  {
    return static::METHOD;
  }

  /**
   * @param $id
   * @return CalculatorRequest
   */
  public function setSenderCityId($id)
  {
    $this->senderCityId = $id;
    return $this;
  }

  /**
   * @param $id
   * @return CalculatorRequest
   */
  public function setReceiverCityId($id)
  {
    $this->receiverCityId = $id;
    return $this;
  }

  /**
   * @param $code
   * @return CalculatorRequest
   */
  public function setSenderCityPostCode($code)
  {
    $this->senderCityPostCode = $code;
    return $this;
  }

  /**
   * @param $code
   * @return CalculatorRequest
   */
  public function setReceiverCityPostCode($code)
  {
    $this->receiverCityPostCode = $code;
    return $this;
  }

  /**
   * @param $id
   * @return CalculatorRequest
   */
  public function setTariffId($id)
  {
    $this->tariffList = null;
    $this->tariffId = $id;
    return $this;
  }

  /**
   * @param $id
   * @param $priority
   * @return CalculatorRequest
   */
  public function addTariffToList($id, $priority)
  {
    $this->tariffId = null;
    $this->tariffList[] = compact('id', 'priority');
    return $this;
  }

  /**
   * @param $id
   * @return CalculatorRequest
   */
  public function setModeId($id)
  {
    $this->modeId = $id;
    return $this;
  }

  /**
   * @param $good
   * @return $this
   */
  public function addGood($good)
  {
    $this->goods[] = $good;
    return $this;
  }

  /**
   * @return array
   */
  public function getBody()
  {
    return array_filter([
      'dateExecute'          => $this->dateExecute->format('Y-m-d'),
      'authLogin'            => $this->authLogin,
      'secure'               => $this->secure,
      'senderCityId'         => $this->senderCityId,
      'senderCityPostCode'   => $this->senderCityPostCode,
      'receiverCityId'       => $this->receiverCityId,
      'receiverCityPostCode' => $this->receiverCityPostCode,
      'tariffId'             => $this->tariffId,
      'tariffList'           => $this->tariffList,
      'modeId'               => $this->modeId,
      'goods'                => $this->goods,
      'version'              => '1.0',
    ]);
  }
}